<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    $_SESSION['this_route'] = 'order_history.php';
    $pageSize = 10;
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div class="text-center mt-3"><h3>การจอง</h3></div>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="col-1 text-center" scope="col">#</th>
                <th class="col-2 text-center" scope="col">ร้านอาหาร</th>
                <th class="col-2 text-center" scope="col">วันที่เข้าใช้บริการ</th>
                <th class="col-2 text-center" scope="col">เวลาเข้า-ออก</th>
                <th class="col-2 text-center" scope="col">สถานะ</th>
                <th class="col-auto text-center" scope="col">จัดการ</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sql = "SELECT `orders`.*,`shop`.`shop_name`,`payment`.* FROM `orders`
INNER JOIN `member` ON `member`.`member_id` = `orders`.`member_id`
INNER JOIN `shop` ON `shop`.`shop_id` = `orders`.`shop_id`
INNER JOIN `payment` ON `payment`.`payment_id`= `orders`.`payment_id`
WHERE `orders`.`member_id` = '{$_SESSION['mid']}' 
ORDER BY `orders`.`order_id` DESC
LIMIT " . $pageSize . " OFFSET " . ($page - 1);
            $result = $conn->query($sql);
            while ($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <td class="text-center"><?php echo $row['order_id'] ?></td>
                    <td><?php echo $row['shop_name'] ?></td>
                    <td><?php echo $row['order_date'] ?></td>
                    <td><?php echo $row['order_time_in'] . ' - ' . $row['order_time_out'] ?></td>
                    <td><?php
                        if ($row['payment_code'] == $NOT_PAY_STATUS) {
                            ?>
                            <span style="color: #909090"><?php echo $row['payment_name'] . '(' . $row['payment_code'] . ')' ?></span>
                            <?php
                        } else if ($row['payment_code'] == $PAY_BY_BANK) {
                            ?>
                            <span style="color: #46B624"><?php echo $row['payment_name'] . '(' . $row['payment_code'] . ')' ?></span>
                            <?php
                        } else if ($row['payment_code'] == $PAY_BY_MONEY) {
                            ?>
                            <span style="color: #2B76AF"><?php echo $row['payment_name'] . '(' . $row['payment_code'] . ')' ?></span>
                            <?php
                        }
                        ?></td>
                    <td>
                        <a href="./order_detail.php?order_id=<?php echo $row['order_id'] ?>">
                            <button class="btn btn-info mr-2">ดูรายละเอียด</button>
                        </a>
                        <?php
                        if ($row['payment_code'] != $NOT_PAY_STATUS) {
                            $sql = "SELECT COUNT(*) AS NUM FROM review_shops WHERE order_id='{$row['order_id']}' 
                                           AND member_id = '{$_SESSION['mid']}'";
                            $result_count = $conn->query($sql);
                            $reviews = $result_count->fetch_assoc();
                            if($reviews['NUM'] == 0){
                            ?>
                            <a href="./report_shop.php?order_id=<?php echo $row['order_id']?>&shop_id=<?php echo $row['shop_id'] ?>">
                                <button class="btn btn-dark ">รายงานผู้ดูแล</button>
                            </a>
                            <?php
                            }
                        }
                        ?>

                    </td>

                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <nav aria-label="Page navigation example">

            <ul class="pagination justify-content-end">

                <?php
                $sql = "SELECT COUNT(*) AS NUM FROM `orders`
INNER JOIN `member` ON `member`.`member_id` = `orders`.`member_id`
INNER JOIN `shop` ON `shop`.`shop_id` = `orders`.`shop_id`
INNER JOIN `payment` ON `payment`.`payment_id`= `orders`.`payment_id`
WHERE `orders`.`member_id` = '{$_SESSION['mid']}' ";
                $result_num = $conn->query($sql);
                $row = $result_num->fetch_assoc();
                ?>
                <li class="page-item <?php echo $page <= 1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $page - 1; ?>">Previous</a>
                </li>
                <?php
                for ($i = 0; $i < ceil($row['NUM'] / $pageSize); $i++) {
                    ?>
                    <li class="page-item <?php echo ($page == ($i + 1)) ? 'active' : '' ?>"><a
                                href="?page=<?= $i + 1; ?>" class="page-link" href="#"><?php echo $i + 1; ?></a></li>
                    <?php
                }
                ?>

                <li class="page-item <?php echo  ceil($row['NUM'] / $pageSize) == ($page) || $row['NUM'] == 0 ? 'disabled' : '' ?>">
                    <a class="page-link " href="?page=<?= $page + 1; ?>">Next</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<?php include './layout/footer.php' ?>
</body>
</html>
