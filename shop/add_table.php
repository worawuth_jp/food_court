<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'shop/food.php';
    if(isset($_POST['submit'])){
        $shop_id = $_SESSION['shop_id'];
        $table_no = $_POST['table_no'];
        $table_num = $_SESSION['table_num'];
        $sql = "INSERT INTO `tables`(`shop_id`,`table_no`,`table_num`) 
VALUES ('$shop_id','$table_no','$table_num')";
        $result = $conn->query($sql);
        if($result){
            alert('เพิ่มโต๊ะอาหารสำเร็จ','../shop/booking.php');
        }
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">เพิ่มโต๊ะอาหาร</h1>
            </div>

            <div class="self-content">
                <form method="post" class="col-md-6 p-0" action="./add_table.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">หมายเลยโต๊ะ</label>
                        <input type="text" name="table_no" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">จำนวนที่นั่ง/โต๊ะ</label>
                        <input type="number" name="table_num" class="form-control col-md-4" min="0" max="999" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">เพิ่มโต๊ะอาหาร</button>
                </form>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>