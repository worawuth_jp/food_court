<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'shop/food.php';
    $food_id = '';
    if(isset($_GET['food_id'])){
        $food_id = $_GET['food_id'];
    }

    if(isset($_POST['submit'])){
//        print_r(json_encode($_POST));
//        print_r(json_encode($_FILES));

        $food_id = $_POST['food_id'];
        $food_name = $_POST['food_name'];
        $food_detail = $_POST['food_detail'];
        $food_price = is_numeric($_POST['food_price']) ? $_POST['food_price'] : 0.00;
        $food_size = $_POST['food_size'];
        $food_type = $_POST['food_type'];

        $target_dir = "../image/foods_img/";
        $target_file ='food_'.generateRandomString().'_'. $_SESSION['shop_id'].'_'.date('YmdHis');
        $food_img = $target_file.'.png';
        if (move_uploaded_file($_FILES["food_img"]["tmp_name"],$target_dir.$food_img )) {
            $sql = "UPDATE foods 
SET food_name='$food_name',food_detail='$food_detail',food_price='$food_price',food_size='$food_size',food_type='$food_type',food_img='$food_img'
WHERE food_id='$food_id'";
            $result = $conn->query($sql);
            if($result){
                alert('เพิ่มเมนูสำเร็จ','../shop/food.php');
            }
        } else{
            alert('เกิดข้อผิดพลาด');
        }
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">เพิ่มเมนูอาหาร</h1>
            </div>

            <?php
            $sql = "SELECT * FROM foods WHERE food_id = '$food_id'";
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();
            ?>

            <div class="self-content">
                <form class="col-md-9 ml-auto mr-auto" method="post" action="./add_food.php" enctype="multipart/form-data">
                    <input type="hidden" name="food_id" value="<?php echo $food_id;?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">ชื่อเมนูอาหาร</label>
                        <input type="text" name="food_name" class="form-control"
                               value="<?php echo $row['food_name'];?>"
                               id="exampleInputEmail1" aria-describedby="emailHelp">
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">ราคา</label>
                        <input type="text" name="food_price" class="form-control"
                               value="<?php echo $row['food_price'];?>"
                               id="exampleInputEmail1" aria-describedby="emailHelp">
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">ขนาด</label>
                        <input type="text" name="food_size" class="form-control"
                               value="<?php echo $row['food_size'];?>"
                               id="exampleInputEmail1" aria-describedby="emailHelp">
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">รายละเอียด</label>
                        <textarea name="food_detail" class="form-control"><?php echo $row['food_price'];?></textarea>
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">หมดหมู่อาหาร</label>
                        <select class="form-control" name="food_type" >
                            <?php
                            $sql = "SELECT * FROM type_foods WHERE shop_id='{$_SESSION['shop_id']}'";
                            $result_foods = $conn->query($sql);
                            $i=0;
                            while ($row1 = $result_foods->fetch_assoc()){
                                ?>
                                <option <?php echo $row1['type_food_id'] == $row['type_food_id'] ? 'selected' : ''?> value="<?= $row1['type_food_id'] ?>"><?= $row1['type_food_name'] ?></option>
                                <?php
                                $i++;
                            }
                            ?>
                        </select>
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>

                    <div class="form-group">
                        <img class="img-fluid col-md-12" src="../image/foods_img/<?php echo $row['food_img']?>" />
                        <label for="exampleInputEmail1">แก้ไขเมนูอาหาร</label>
                        <input type="file" accept="image/*" name="food_img" />
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>
                    <div class="col-md-12 pl-0 pr-0 text-center">
                        <button type="submit" name="submit" class="btn btn-primary col-md-12">เพิ่มเมนูอาหาร</button>
                    </div>
                </form>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>