<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'shop/food.php';
    if(isset($_POST['submit'])){
        $category = $_POST['category'];
        $shop_id = $_SESSION['shop_id'];
        $sql = "INSERT INTO `type_foods`(`type_food_name`, `shop_id`) 
VALUES ('$category','$shop_id')";
        $result = $conn->query($sql);
        if($result){
            alert('เพิ่มหมวดหมู่สำเร็จ','../shop/food.php');
        }
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">เพิ่มหมวดหมู่อาหาร</h1>
            </div>

            <div class="self-content">
                <form method="post" action="./add_category_food.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">หมวดหมู่อาหาร</label>
                        <input type="text" name="category" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
<!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">เพิ่มหมวดหมู่อาหาร</button>
                </form>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>