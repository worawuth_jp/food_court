<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'shop/report_customer.php';


    $order_id = null;

    if(isset($_GET['order_id']) || isset($_POST['order_id'])){
        $order_id = $_GET['order_id'];
    }else{
        alert('ข้อมูลไม่ครบถ้วน ทำรายการอีกครั้ง','../'.$_SESSION['this_route']);
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Dashboard</h1>
            </div>

            <div class="self-content">
                <form action="./submit_report_customer.php" method="post">
                    <input type="hidden" name="order_id" value="<?php echo $order_id?>">
                    <div class="form-group col-md-7 ml-auto mr-auto">
                        <label for="exampleFormControlTextarea1">รายงานความคิดเห็น</label>
                        <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                    </div>

                    <div class="col-md-12 text-center">
                        <button class="col-md-7 ml-auto mr-auto btn btn-primary">ส่งความคิดเห็น</button>
                    </div>
                </form>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>