<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'shop/booking.php';
    $pageSize = 10;
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">โต๊ะอาหาร</h1>
            </div>

            <div class="self-content">
                <div class="text-center mt-3"><h3>โต๊ะอาหาร</h3></div>
                <div class="col-md-12 text-right">
                    <a href="./add_table.php" class="btn btn-primary">เพิ่มโต๊ะอาหาร</a>
                </div>
                <div class="col-md-12 mt-2">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class=" text-center" scope="col">#</th>
                            <th class="col-2 text-center" scope="col">หมายเลขโต๊ะ</th>
                            <th class="col-2 text-center" scope="col">จำนวนที่นั่ง/คน</th>
                            <th class="col-auto text-center" scope="col">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $shop_id = $_SESSION['shop_id'];
                        $sql = "SELECT * FROM tables 
INNER JOIN shop ON shop.shop_id = tables.shop_id
WHERE `shop`.`shop_id`='$shop_id'
ORDER BY `shop`.`shop_id` DESC
LIMIT " . $pageSize . " OFFSET " . ($page - 1);
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $row['table_id'] ?></td>
                                <td><?php echo $row['table_no'] ?></td>
                                <td class="text-center"><?php echo $row['table_num']?></td>
                                <td class="text-center">
                                    <a href="./booking_detail.php?table_id=<?php echo $row['table_id'] ?>">
                                        <button class="btn btn-info mr-2">ดูรายละเอียด</button>
                                    </a>

                                </td>

                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">

                        <ul class="pagination justify-content-end">

                            <?php
                            $sql = "SELECT COUNT(*) AS NUM FROM tables
INNER JOIN `shop` ON `shop`.`shop_id` = `tables`.`shop_id`
WHERE `shop`.`shop_id` = '$shop_id' ";
                            $result_num = $conn->query($sql);
                            $row = $result_num->fetch_assoc();
                            ?>
                            <li class="page-item <?php echo $page <= 1 ? 'disabled' : '' ?>">
                                <a class="page-link" href="?page=<?= $page - 1; ?>">Previous</a>
                            </li>
                            <?php
                            for ($i = 0; $i < ceil($row['NUM'] / $pageSize); $i++) {
                                ?>
                                <li class="page-item <?php echo ($page == ($i + 1)) ? 'active' : '' ?>"><a
                                        href="?page=<?= $i + 1; ?>" class="page-link" href="#"><?php echo $i + 1; ?></a></li>
                                <?php
                            }
                            ?>

                            <li class="page-item <?php echo ceil($row['NUM'] / $pageSize) == ($page) || $row['NUM'] == 0 ? 'disabled' : '' ?>">
                                <a class="page-link " href="?page=<?= $page + 1; ?>">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>