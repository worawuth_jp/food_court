<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'shop/booking.php';
    $pageSize = 10;
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }

    $table_id = $_GET['table_id'];
    if(!isset($_GET['table_id'])){
        alert('รายการผิดพลาด','booking.php');
    }

    if(isset($_GET['cancel'])){
        $order_table_id = $_GET['order_table_id'];
        $sql = "DELETE FROM order_tables WHERE order_table_id='$order_table_id'";
        if($conn->query($sql)){
            alert('ยกเลิกโต๊ะสำเร็จ','booking_detail.php?table_id='.$table_id);
        }
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">การจองโต๊ะ</h1>
            </div>

            <div class="self-content">
                <div class="text-center mt-3"><h3>รายการจองโต๊ะหมายเลข <?php echo $_GET['table_id']?></h3></div>

                <div class="col-md-12 mt-2">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class=" text-center" scope="col">#</th>
                            <th class="col-2 text-center" scope="col">ลูกค้า</th>
                            <th class="col-3 text-center" scope="col">วันที่</th>
                            <th class="col-auto text-center" scope="col">เวลาเข้า-ออก</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT * FROM order_tables 
INNER JOIN tables ON tables.table_id = order_tables.table_id
INNER JOIN orders ON orders.order_id = order_tables.order_id
INNER JOIN member ON member.member_id = orders.member_id
WHERE `tables`.`table_id`='$table_id'
ORDER BY `order_tables`.`order_table_id` DESC
LIMIT " . $pageSize . " OFFSET " . ($page - 1);
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $row['order_id'] ?></td>
                                <td><?php echo '('.$row['member_id'].')'.$row['member_name'] ?></td>
                                <td class="text-center"><?php echo $row['order_date'] ?></td>
                                <td class="text-center"><?php echo $row['order_time_in']. ' - '.$row['order_time_out']?></td>
                                <td class="text-center">
                                    <?php
                                    $dateString = $row['order_date'].' '.$row['order_time_out'];
                                    $date1 = new DateTime($dateString);
                                    $dateNow = new DateTime();
                                    if($dateNow < $date1){
                                    ?>
                                    <a href="?cancel=1&order_table_id=<?php echo $row['order_table_id'] ?>&table_id=<?php echo $table_id?>" >
                                        <button class="btn btn-warning" data-toggle="tooltip"
                                                data-placement="top"
                                                title="ยกเลิก">
                                            <i class="fas fa-window-close"></i>
                                        </button>

                                    </a>
                                    <?php
                                    }
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">

                        <ul class="pagination justify-content-end">

                            <?php
                            $sql = "SELECT COUNT(*) AS NUM FROM order_tables 
INNER JOIN tables ON tables.table_id = order_tables.table_id
INNER JOIN orders ON orders.order_id = order_tables.order_id
WHERE `tables`.`table_id`='$table_id'";
                            $result_num = $conn->query($sql);
                            $row = $result_num->fetch_assoc();
                            ?>
                            <li class="page-item <?php echo $page <= 1 ? 'disabled' : '' ?>">
                                <a class="page-link" href="?page=<?= $page - 1; ?>">Previous</a>
                            </li>
                            <?php
                            for ($i = 0; $i < ceil($row['NUM'] / $pageSize); $i++) {
                                ?>
                                <li class="page-item <?php echo ($page == ($i + 1)) ? 'active' : '' ?>"><a
                                        href="?page=<?= $i + 1; ?>" class="page-link" href="#"><?php echo $i + 1; ?></a></li>
                                <?php
                            }
                            ?>

                            <li class="page-item <?php echo ceil($row['NUM'] / $pageSize) == ($page) || $row['NUM'] == 0 ? 'disabled' : '' ?>">
                                <a class="page-link " href="?page=<?= $page + 1; ?>">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>