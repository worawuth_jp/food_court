<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    $_SESSION['this_route'] = 'order_detail.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div class="text-center mt-3 col-md-12"><h4>รายละเอียดการจอง</h4></div>
    <div class="col-md-12 text-center" style="color: darkblue">
        <?php
        $order_id = $_GET['order_id'];
        $sql = "SELECT * FROM orders INNER JOIN shop ON shop.shop_id = orders.shop_id
WHERE orders.order_id='$order_id'";
        $result_order = $conn->query($sql);
        $record = $result_order->fetch_assoc();
        $d = new DateTime($record['order_date']);
        $time_in = new DateTime($record['order_time_in']);
        $time_out = new DateTime($record['order_time_out']);
        $shop_id = $record['shop_id'];
        ?>
        <h5><?= $record['shop_name'] ?> (<?= $d->format('d/m/Y') ?>) เวลา : <?= $time_in->format('H:i') ?> - <?= $time_out->format('H:i') ?></h5>
    </div>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="col-1 text-center" scope="col">#</th>
                <th class="col-2 text-center" scope="col">หมายเลขโต๊ะ</th>
                <th class="col-2 text-center" scope="col">จำนวนที่นั่ง/โต๊ะ</th>

            </tr>
            </thead>
            <tbody>
            <?php

            $sql = "SELECT * FROM `orders` 
INNER JOIN order_tables ON order_tables.order_id = orders.order_id
INNER JOIN tables ON tables.table_id = order_tables.table_id
WHERE orders.order_id = '$order_id' ";
            $result = $conn->query($sql);
            while ($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <td class="text-center"><?php echo $row['table_id'] ?></td>
                    <td><?php echo $row['table_no'] ?></td>
                    <td><?php echo $row['table_num'] ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>

    <div class="col-md-12 mt-3 text-center" style="color: darkblue">
        <h5>รายการอาหาร</h5>
    </div>

    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="col-1 text-center" scope="col">#</th>
                <th class="col-4 text-center" scope="col">รายการอาหาร</th>
                <th class="col-1 text-center" scope="col">จำนวน</th>
                <th class="col-2 text-center" scope="col">ราคา</th>
                <th class="col-auto text-center" scope="col">รวม</th>

            </tr>
            </thead>
            <tbody>
            <?php

            $sql = "SELECT order_foods.*,foods.food_name FROM `orders` 
INNER JOIN order_foods ON order_foods.order_id = orders.order_id
INNER JOIN foods ON foods.food_id = order_foods.food_id
WHERE orders.order_id = '$order_id' ";
            $result = $conn->query($sql);
            $sum_total = 0;
            while ($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <td class="text-center"><?php echo $row['food_id'] ?></td>
                    <td class="text-center"><?php echo $row['food_name'] ?></td>
                    <td class="text-center"><?php echo $row['food_num'] ?></td>
                    <td class="text-center"><?php echo $row['food_price'] ?></td>
                    <td class="text-right"><?php $sum_total+=$row['food_price']*$row['food_num']; echo number_format($row['food_price']*$row['food_num'],2) ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td colspan="4" class="text-right">รวมทั้งหมด</td>
                <td><?= number_format($sum_total,2)?></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-12 text-right">
        <a href="./order_history.php">
            <button class="btn btn-warning ml-2">หน้าการจองของฉัน</button>
        </a>
        <?php
        $sql = "SELECT COUNT(*) AS NUM FROM review_shops WHERE order_id='$order_id' AND member_id = '{$_SESSION['mid']}'";
        $result_count = $conn->query($sql);
        $reviews = $result_count->fetch_assoc();
        if($reviews['NUM'] == 0){
        ?>
        <a href="./report_shop.php?order_id=<?php echo $order_id?>&shop_id=<?php echo $shop_id ?>">
            <button class="btn btn-dark ml-2">รายงานผู้ดูแล</button>
        </a>
        <?php } ?>
    </div>
</div>
<?php include './layout/footer.php' ?>
</body>
</html>
