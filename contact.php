
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    save_route('contact.php',$_SESSION);
    $_SESSION['this_route'] = 'contact.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div class="text-center mt-3"><h3>ข่าวสาร</h3></div>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
