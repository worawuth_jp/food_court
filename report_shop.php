
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    $_SESSION['this_route'] = 'report_shop.php';


    $order_id = null;

    if(isset($_GET['order_id'])){
        $order_id = $_GET['order_id'];
    }else{
        alert('ข้อมูลไม่ครบถ้วน ทำรายการอีกครั้ง',$_SESSION['this_route']);
    }
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div class="text-center mt-3"><h3>รายงานผู้ดูแล</h3></div>

    <form action="./submit_report_shop.php" method="post">
        <input type="hidden" name="order_id" value="<?php echo $order_id?>">
        <div class="form-group col-md-7 ml-auto mr-auto">
            <label for="exampleFormControlTextarea1">รายงานความคิดเห็น</label>
            <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
        </div>

        <div class="col-md-12 text-center">
            <button class="col-md-7 ml-auto mr-auto btn btn-primary">ส่งความคิดเห็น</button>
        </div>
    </form>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
