-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2021 at 03:56 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `about_id` int(11) NOT NULL,
  `about_header` varchar(100) NOT NULL,
  `about_content` varchar(1000) NOT NULL,
  `about_video` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`about_id`, `about_header`, `about_content`, `about_video`) VALUES
(1, 'Restaurant Center', 'สำหรับจุดเริ่มต้นแรกและชื่อที่มาของ Restaurant Center นั้น มาจากความตั้งใจที่อยากร้านอาหารต่างๆ จากร้านที่ตั้งอยู่บนห้างสรรพสินค้ามาเสิร์ฟอยู่ริมถนนแถบชานเมืองในราคาแสนเป็นมิตร แต่ได้อิ่มอร่อยทั้งปริมาณและคุณภาพ รวมถึงรสชาติที่ถูกปากให้คนทั่วไปแวะเข้ามาทานได้ บรรยากาศในร้านค่อนข้างเป็นระเบียบ ดูสบายตาทีเดียว ตัวหลังคาสูงปลอดโปร่ง ปูด้วยแผ่นเมทัลชีทกันความร้อนอีกชั้น แถมยังอัพเกรดความน่านั่งให้มากขึ้น ด้วยการตกแต่งสไตล์ Vintage Loft อิฐเปลือย โดยร้านอาหารจะมีหลากหลายสไตล์ และมีมากกว่า 50 เมนู', '13_03_2021_about.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `about_img`
--

CREATE TABLE `about_img` (
  `about_img_id` int(11) NOT NULL,
  `about_img` varchar(100) NOT NULL,
  `about_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_img`
--

INSERT INTO `about_img` (`about_img_id`, `about_img`, `about_id`) VALUES
(5, '13_03_2021_1.jpg', 1),
(6, '13_03_2021_2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `bnID` int(11) NOT NULL,
  `bn_name` varchar(50) NOT NULL,
  `bn_account_name` varchar(75) NOT NULL,
  `bn_account_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bnID`, `bn_name`, `bn_account_name`, `bn_account_id`) VALUES
(1, 'ไทยพาณิชย์', 'นาย ก นามสกุล ข', '111-1121-12-1');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE `foods` (
  `food_id` int(11) NOT NULL,
  `food_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `food_detail` text NOT NULL,
  `food_img` varchar(100) NOT NULL,
  `food_price` decimal(20,2) NOT NULL,
  `food_size` varchar(50) NOT NULL,
  `type_food_id` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`food_id`, `food_name`, `food_detail`, `food_img`, `food_price`, `food_size`, `type_food_id`) VALUES
(1, 'สเต็กเนื้อสันนอกออสเตรเลีย', 'สเต็กเนื้อสันนอกออสเตรเลีย', 'สเต็กเนื้อสันนอกออสเตรเลีย.jpg', '220.00', 'S', 1),
(2, 'สเต็กเนื้อสันนอกออสเตรเลีย', 'สเต็กเนื้อสันนอกออสเตรเลีย', 'สเต็กเนื้อสันนอกออสเตรเลีย.jpg', '250.00', 'M', 1),
(3, 'สเต็กเนื้อสันนอกออสเตรเลีย', 'สเต็กเนื้อสันนอกออสเตรเลีย', 'สเต็กเนื้อสันนอกออสเตรเลีย.jpg', '290.00', 'L', 1),
(4, 'กะเพราหมู', 'กะเพราหมู', 'DSC02049-1-1024x684.jpg', '60.00', 'ธรรมดา', 12),
(5, 'กะเพราหมู', 'กะเพราหมู', 'DSC02049-1-1024x684.jpg', '80.00', 'พิเศษ', 12),
(6, 'test1', 'sss', '20211004230304.png', '10.00', 's', 14);

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `information_id` int(11) NOT NULL,
  `information_title` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `information_detail` text NOT NULL,
  `information_img` varchar(100) NOT NULL,
  `information_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `type_member_id` int(5) NOT NULL,
  `member_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `member_lastname` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `member_username` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `member_password` int(50) NOT NULL,
  `member_date` datetime NOT NULL,
  `member_email` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `member_address` text CHARACTER SET utf8mb4 NOT NULL,
  `member_phone` varchar(50) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `type_member_id`, `member_name`, `member_lastname`, `member_username`, `member_password`, `member_date`, `member_email`, `member_address`, `member_phone`) VALUES
(11370, 1, 'admin', '444', 'admin', 1, '2018-11-09 08:48:00', 'aom@gmail.com', 'บ้านแอดมิน ครับ', '038'),
(11388, 2, 'ร้านอาหารตามสั่ง', 'ร้านใจดี', 'shop', 1, '2021-02-09 20:47:46', 'test@gmail.com', 'vvvv ออ', 'ghfh'),
(11389, 3, 'hjhgj', 'jhj', 'test', 1, '0000-00-00 00:00:00', 'hfgh@gmail.com', 'hjgjg', '05988'),
(11392, 2, 'ร้านอาหารใต้', 'จ่าดำ', 'shop2', 1, '2021-10-01 11:02:40', 'hfgh@gmail.com', '', 'dfgdfgdfgd'),
(11393, 2, 'อิสลาม', 'ราตี', 'shopone', 1, '2021-10-01 11:04:06', 'test@gmail.com', 'fdsf', 'fgfg'),
(11395, 2, 'test', 'shop', 'testshop', 1, '2021-10-05 08:53:19', 'test@test', 'testAddress', '0000');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `order_num_person` int(11) NOT NULL DEFAULT 0,
  `order_date` date DEFAULT NULL,
  `order_time_in` time DEFAULT NULL,
  `order_time_out` time DEFAULT NULL,
  `payment_id` int(11) NOT NULL DEFAULT 1,
  `member_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `shop_id`, `order_num_person`, `order_date`, `order_time_in`, `order_time_out`, `payment_id`, `member_id`) VALUES
(1, 11397, 2, '2021-10-04', '02:00:00', '03:00:00', 2, 11389),
(2, 11398, 3, '2021-10-03', '00:00:00', '00:00:00', 3, 11389),
(11, 11397, 8, '2021-10-05', '09:00:00', '10:00:00', 1, 11389);

-- --------------------------------------------------------

--
-- Table structure for table `order_foods`
--

CREATE TABLE `order_foods` (
  `order_food_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `food_num` int(11) NOT NULL DEFAULT 0,
  `food_price` decimal(20,2) NOT NULL DEFAULT 0.00,
  `food_comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_foods`
--

INSERT INTO `order_foods` (`order_food_id`, `order_id`, `food_id`, `food_num`, `food_price`, `food_comment`) VALUES
(1, 1, 1, 1, '220.00', NULL),
(2, 1, 2, 1, '250.00', NULL),
(3, 2, 4, 1, '60.00', NULL),
(16, 11, 1, 2, '220.00', NULL),
(17, 11, 2, 1, '250.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_tables`
--

CREATE TABLE `order_tables` (
  `order_table_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_tables`
--

INSERT INTO `order_tables` (`order_table_id`, `order_id`, `table_id`) VALUES
(1, 1, 1),
(2, 2, 3),
(3, 3, 1),
(11, 11, 1),
(12, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `payment_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `payment_code` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_name`, `payment_code`) VALUES
(1, 'รอเข้าใช้บริการ', 'NOT_PAY'),
(2, 'ชำระผ่านบัญชีธนาคาร', 'PAY_BY_BANK'),
(3, 'เงินสด', 'PAY_BY_MONEY');

-- --------------------------------------------------------

--
-- Table structure for table `review_customers`
--

CREATE TABLE `review_customers` (
  `review_customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `review_customer_comment` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `review_customers`
--

INSERT INTO `review_customers` (`review_customer_id`, `order_id`, `review_customer_comment`) VALUES
(1, 1, '1234');

-- --------------------------------------------------------

--
-- Table structure for table `review_shops`
--

CREATE TABLE `review_shops` (
  `review_shop_id` int(11) NOT NULL,
  `review_shop_comment` longtext NOT NULL,
  `member_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `review_shops`
--

INSERT INTO `review_shops` (`review_shop_id`, `review_shop_comment`, `member_id`, `order_id`) VALUES
(1, '1234', 11389, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shop_id` int(11) NOT NULL,
  `shop_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `location` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT 'ตำแหน่งร้าน',
  `pic_shop` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT 'รูปร้าน',
  `member_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shop_id`, `shop_name`, `location`, `pic_shop`, `member_id`) VALUES
(11397, 'ร้านอาหาร1', '', '37265105_10156534650187640_6497132302949154816_n-1.jpg', 11388),
(11398, 'ร้านอาหาร2', '', '161308040_1415026085535779_9214726391013472360_n.jpg', 11392),
(11399, 'test', '', 'shop_20211005085319.png', 11395);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `table_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `table_no` varchar(50) NOT NULL,
  `table_num` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`table_id`, `shop_id`, `table_no`, `table_num`) VALUES
(1, 11397, 'A1', 4),
(2, 11397, 'A2', 4),
(3, 11398, 'A1', 4),
(4, 11398, 'A2', 4),
(5, 11399, 'A01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `type_foods`
--

CREATE TABLE `type_foods` (
  `type_food_id` int(11) NOT NULL,
  `type_food_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `shop_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_foods`
--

INSERT INTO `type_foods` (`type_food_id`, `type_food_name`, `shop_id`) VALUES
(1, 'Steak', 11397),
(4, 'ยำ', 11397),
(5, 'ต้มยำ', 11398),
(6, 'เครื่องดื่ม', 11398),
(7, 'แกง', 11398),
(8, 'แกงใต้', 11398),
(9, 'แกงอีสาน', 11398),
(10, 'แกงเหนือ', 11398),
(11, 'อาหารอิสลาม', 11398),
(12, 'ตามสั่ง', 11398),
(14, 'test', 11397);

-- --------------------------------------------------------

--
-- Table structure for table `type_member`
--

CREATE TABLE `type_member` (
  `type_member_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_member`
--

INSERT INTO `type_member` (`type_member_id`, `name`) VALUES
(1, 'admin'),
(2, 'ร้านค้า'),
(3, 'สมาชิก');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `about_img`
--
ALTER TABLE `about_img`
  ADD PRIMARY KEY (`about_img_id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bnID`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`food_id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `type_member` (`type_member_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `shop_id` (`shop_id`),
  ADD KEY `payment_id` (`payment_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `order_foods`
--
ALTER TABLE `order_foods`
  ADD PRIMARY KEY (`order_food_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `food_id` (`food_id`),
  ADD KEY `food_num` (`food_num`),
  ADD KEY `food_price` (`food_price`);

--
-- Indexes for table `order_tables`
--
ALTER TABLE `order_tables`
  ADD PRIMARY KEY (`order_table_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `table_id` (`table_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `payment_code` (`payment_code`);

--
-- Indexes for table `review_customers`
--
ALTER TABLE `review_customers`
  ADD PRIMARY KEY (`review_customer_id`);

--
-- Indexes for table `review_shops`
--
ALTER TABLE `review_shops`
  ADD PRIMARY KEY (`review_shop_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shop_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`table_id`),
  ADD KEY `shop_id` (`shop_id`),
  ADD KEY `table_no` (`table_no`);

--
-- Indexes for table `type_foods`
--
ALTER TABLE `type_foods`
  ADD PRIMARY KEY (`type_food_id`);

--
-- Indexes for table `type_member`
--
ALTER TABLE `type_member`
  ADD PRIMARY KEY (`type_member_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `about_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `about_img`
--
ALTER TABLE `about_img`
  MODIFY `about_img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bnID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
  MODIFY `food_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11353;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11396;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order_foods`
--
ALTER TABLE `order_foods`
  MODIFY `order_food_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order_tables`
--
ALTER TABLE `order_tables`
  MODIFY `order_table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `review_customers`
--
ALTER TABLE `review_customers`
  MODIFY `review_customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `review_shops`
--
ALTER TABLE `review_shops`
  MODIFY `review_shop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11400;

--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `type_foods`
--
ALTER TABLE `type_foods`
  MODIFY `type_food_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `type_member`
--
ALTER TABLE `type_member`
  MODIFY `type_member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`type_member_id`) REFERENCES `type_member` (`type_member_id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `review_shops`
--
ALTER TABLE `review_shops`
  ADD CONSTRAINT `review_shops_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_shops_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tables`
--
ALTER TABLE `tables`
  ADD CONSTRAINT `tables_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
