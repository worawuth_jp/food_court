
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    $_SESSION['this_route'] = 'login.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';
//echo $_SESSION['route'];
?>
<div class="container self-container">
    <div class="text-center mt-3"><h3>ลงทะเบียนสมาชิก</h3></div>

    <form class="col-md-6 ml-auto mr-auto mb-5" method="post" action="./user_register.php">

        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Username">
            <!--            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Confirm Password</label>
            <input type="password" name="conf_password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">ชื่อ-นามสกุล</label>
            <div class="row pl-3 pr-3">
                <input type="text" name="firstname" class="form-control col-md-5" id="exampleInputEmail1" aria-describedby="emailHelp"
                       placeholder="ชื่อจริง">

                <input type="text" name="lastname" class="form-control col-md-5 ml-3" id="exampleInputEmail1" aria-describedby="emailHelp"
                       placeholder="นามสกุล">
            </div>
            <!--            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">เบอร์ติดต่อ</label>
            <input type="text" name="phone" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Phone number">
            <!--            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Email">
            <!--            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Address</label>
            <textarea name="address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                      placeholder="Email"></textarea>
            <!--            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>

        <div class="row text-center pl-3 pr-3">
            <a href="./login.php" class="btn btn-secondary col-md-5">ย้อนกลับ</a>
            <button type="submit" name="member" class="btn btn-primary col-md-5 ml-auto">Submit</button>
        </div>


    </form>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
