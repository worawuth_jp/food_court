<?php
$numCart = countInCart($_SESSION);
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed">
    <a class="navbar-brand" href="#"><i class="fas fa-utensils"></i><?= ' ' ?>Restaurants Center</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto pr-3">
            <li class="nav-item <?php echo isActive($_SESSION, 'index.php') ?>">
                <a class="nav-link" href="./">หน้าหลัก <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item <?php echo isActive($_SESSION, 'about.php') ?>">
                <a class="nav-link" href="./about.php">เกี่ยวกับเรา</a>
            </li>
            <li class="nav-item <?php echo isActive($_SESSION, 'shop.php') ?>">
                <a class="nav-link" href="./shop.php">เลือกร้านค้า</a>
            </li>
            <li class="nav-item <?php echo isActive($_SESSION, 'contact.php') ?>">
                <a class="nav-link" href="./contact.php">ติดต่อเรา</a>
            </li>
            <?php
            if (isset($_SESSION['mid'])) {

                ?>
                <li class="nav-item <?php echo isActive($_SESSION, 'order_history.php') ?>">
                    <a class="nav-link" href="./order_history.php">การจองของฉัน</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link self-cart" href="./carts.php">
                        <i class="fas fa-clipboard-list"
                           style="font-size: 28px;color: black"></i>
                        <span class='badge badge-warning' id='lblCartCount'> <?php echo $numCart;?> </span>
                    </a>
                </li>
                <?php
            }
            ?>

            <li class="nav-item <?php echo isActive($_SESSION, 'login.php') ?>">
                <?php
                if (isset($_SESSION['mid'])) {
                    ?>
                    <a class="nav-link" href="./logout.php">
                        <i class=" fa fa-user"></i>
                        <span>ออกจากระบบ</span>
                    </a>
                    <?php
                } else {
                    ?>
                    <a class="nav-link" href="./login.php">
                        <i class=" fa fa-user"></i>
                        <span>เข้าสู่ระบบ</span>
                    </a>
                    <?php
                }
                ?>
            </li>

        </ul>
    </div>
</nav>
