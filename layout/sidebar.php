<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <?php
    if($_SESSION['type_id']==$TYPE_ADMIN){
        ?>
        <div class="sidebar-sticky pt-3">
            <ul class="nav flex-column">
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link --><?php //echo isActive($_SESSION,'admin/index.php')?><!--" href="#">-->
<!--                        สมาชิก <span class="sr-only">(current)</span>-->
<!--                    </a>-->
<!--                </li>-->
                <li class="nav-item <?php echo isActive($_SESSION,'admin/booking.php')?>">
                    <a class="nav-link" href="../admin/booking.php">
                        รายการจอง
                    </a>
                </li>
                <li class="nav-item <?php echo isActive($_SESSION,'admin/shop.php')?>">
                    <a class="nav-link" href="../admin/shop.php">
                        ร้านอาหาร
                    </a>
                </li>
                <li class="nav-item <?php echo isActive($_SESSION,'admin/member.php')?>">
                    <a class="nav-link" href="../admin/member.php">
                        สมาชิก
                    </a>
                </li>

                <li class="nav-item <?php echo isActive($_SESSION, 'login.php') ?>">
                    <?php
                    if (isset($_SESSION['mid'])) {
                        ?>
                        <a class="nav-link" href="../logout.php">
                            <i class=" fa fa-user"></i>
                            <span>ออกจากระบบ</span>
                        </a>
                        <?php
                    } else {
                        ?>
                        <a class="nav-link" href="../login.php">
                            <i class=" fa fa-user"></i>
                            <span>เข้าสู่ระบบ</span>
                        </a>
                        <?php
                    }
                    ?>
                </li>
            </ul>

        </div>
    <?php
    }else if($_SESSION['type_id'] == $TYPE_SHOP){
        ?>
        <div class="sidebar-sticky pt-3">
            <ul class="nav flex-column pt-2">
                <li class="nav-item">
                    <a class="nav-link <?php echo isActive($_SESSION, 'shop/index.php') ?>" href="../shop/index.php">
                        <span data-feather="home"></span>
                        หน้าหลัก <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item <?php echo isActive($_SESSION, 'shop/food.php') ?>">
                    <a class="nav-link" href="../shop/food.php">
                        รายการอาหาร
                    </a>
                </li>
                <li class="nav-item <?php echo isActive($_SESSION, 'shop/orders.php') ?>">
                    <a class="nav-link" href="../shop/orders.php">
                        รายการออเดอร์
                    </a>
                </li>
                <li class="nav-item <?php echo isActive($_SESSION, 'shop/booking.php') ?>">
                    <a class="nav-link" href="../shop/booking.php">
                        การจองโต๊ะ
                    </a>
                </li>

                <li class="nav-item <?php echo isActive($_SESSION, 'login.php') ?>">
                    <?php
                    if (isset($_SESSION['mid'])) {
                        ?>
                        <a class="nav-link" href="../logout.php">
                            <i class=" fa fa-user"></i>
                            <span>ออกจากระบบ</span>
                        </a>
                        <?php
                    } else {
                        ?>
                        <a class="nav-link" href="../login.php">
                            <i class=" fa fa-user"></i>
                            <span>เข้าสู่ระบบ</span>
                        </a>
                        <?php
                    }
                    ?>
                </li>

            </ul>


        </div>
    <?php
    }
    ?>
</nav>