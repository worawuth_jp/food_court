<?php
session_start();
require '../connectDB.php';
require '../utils/index.php';
require '../constants/index.php';
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css"
      integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>

<link rel="stylesheet" href="../styles/admin.css">
<style>
    #sidebarMenu{
        height: 100vh;
        position: fixed;
    }

    .self-content{

    }

    .text-decoration-underline{
        text-decoration: underline;
    }
</style>