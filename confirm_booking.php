<?php
session_start();
require 'connectDB.php';
require 'utils/index.php';
require 'constants/index.php';
if (isset($_POST['submit'])) {
    print_r(json_encode($_POST));
    $carts = $_SESSION['cart'];
    $isDuplicateTime = false;
    foreach ($carts as $shop_id => $foods) {
        $date_in = $_POST['date_' . $shop_id];
        $from_hour = $_POST['from_hour_' . $shop_id];
        $from_min = $_POST['from_min_' . $shop_id];
        $time_in = $from_hour . ':' . $from_min;
        $to_hour = $_POST['to_hour_' . $shop_id];
        $to_min = $_POST['to_min_' . $shop_id];
        $time_out = $to_hour . ':' . $to_min;
        $table = isset($_POST['table_' . $shop_id]) ? $_POST['table_' . $shop_id] : [];
        foreach ($table as $table_id) {
            $sql = "SELECT COUNT(*) AS NUM FROM `order_tables` 
INNER JOIN `orders` ON `orders`.`order_id` = `order_tables`.`order_id`
WHERE `orders`.`order_date` = DATE('2021-10-04') AND `orders`.`shop_id` = '$shop_id' AND `order_tables`.`table_id` = '$table_id'
AND (
(`orders`.`order_time_in` >= TIME('$time_in') AND `orders`.`order_time_in` < TIME('$time_out'))
OR (`orders`.`order_time_out` > TIME('$time_in') AND `orders`.`order_time_out` <= TIME('$time_out'))
OR (TIME('$time_in') >= `orders`.`order_time_in` AND TIME('$time_in') < `orders`.`order_time_out`)
OR (TIME('$time_out') > `orders`.`order_time_in` AND  TIME('$time_out') <= `orders`.`order_time_out`)
);";
            $result_num = $conn->query($sql);
            $list = $result_num->fetch_assoc();
            $num = $list['NUM'];
            if ($num > 0) {
                $isDuplicateTime = true;
                alert('ไม่สามารถจองโต๊ะที่ท่านเลือกได้', 'booking.php');
            }
        }
    }

    if (!$isDuplicateTime) {
        $isFailed = false;
        foreach ($carts as $shop_id => $foods) {
            if ($isFailed) {
                alert('มีบางอย่างผิดพลาด', 'booking.php');
                break;
            }
            $num = ($_POST['num_' . $shop_id] != '') ? $_POST['num_' . $shop_id] : 0;
            $date_in = $_POST['date_' . $shop_id];
            $member_id = $_SESSION['mid'];
            $from_hour = $_POST['from_hour_' . $shop_id];
            $from_min = $_POST['from_min_' . $shop_id];
            $time_in = $from_hour . ':' . $from_min;
            $to_hour = $_POST['to_hour_' . $shop_id];
            $to_min = $_POST['to_min_' . $shop_id];
            $time_out = $to_hour . ':' . $to_min;
            $table = isset($_POST['table_' . $shop_id]) ? $_POST['table_' . $shop_id] : array();
            print_r($table);
            if(count($table) == 0){
                header('location: ./booking.php?empty_table=1&shop_id='.$shop_id);
            }
            $sql = "INSERT INTO `orders`(`shop_id`, `order_num_person`, `order_date`, `order_time_in`, `order_time_out`,`member_id`)
VALUES ('$shop_id','$num','$date_in','$time_in','$time_out','$member_id')";
            print_r($sql . '<br>');
            if (!($conn->query($sql))) {
                $isFailed = true;
            } else {
                $order_id = $conn->insert_id;
                foreach ($foods as $food_id => $food_num) {
                    if ($isFailed) {
                        alert('มีบางอย่างผิดพลาด', 'booking.php');
                        break;
                    }
                    $sql = "SELECT * FROM foods WHERE food_id = '$food_id'";
                    $result_foods = $conn->query($sql);
                    $row = $result_foods->fetch_assoc();
                    $food_price = $row['food_price'];
                    $sql = "INSERT INTO `order_foods`( `order_id`, `food_id`, `food_num`, `food_price`)
VALUES ('$order_id','$food_id','$food_num','$food_price')";
                    $result_insert_food = $conn->query($sql);
                    if (!$result_insert_food) {
                        $isFailed = true;
                    }
                }

                if (count($table) > 0) {
                    foreach ($table as $table_id) {
                        if ($isFailed) {
                            alert('มีบางอย่างผิดพลาด', 'booking.php?shop_id='.$shop_id);
                            break;
                        }
                        $sql = "INSERT INTO `order_tables`(`order_id`, `table_id`)
VALUES ('$order_id','$table_id')";
                        $result_insert_table = $conn->query($sql);
                        if (!$result_insert_table) {
                            $isFailed = true;
                        }
                    }
                }

            }

        }
    }

    if(!$isFailed){
        unset($_SESSION['cart']);
        //header('location: ./booking_success.php');
    }

}