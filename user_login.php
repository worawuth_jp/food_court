<?php
session_start();
require 'connectDB.php';
require 'utils/index.php';
require 'constants/index.php';
$user = '';
$pass = '';
if(isset($_POST['username'])){
    $user = $_POST['username'];
}

if(isset($_POST['password'])){
    $pass = $_POST['password'];
}

$sql = "SELECT COUNT(*) AS NUM,member.member_id,type_member.type_member_id as TYPE_ID,type_member.name as TYPE_NAME,shop.shop_id
FROM member 
INNER JOIN type_member ON type_member.type_member_id = member.type_member_id
LEFT JOIN shop ON shop.member_id = member.member_id
WHERE member.member_username='$user' AND member.member_password='$pass'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
if($row['NUM'] > 0){
    alert('เข้าสู่ระบบสำเร็จ');
    $_SESSION['mid'] = $row['member_id'];
    $_SESSION['type_id'] = $row['TYPE_ID'];
    print_r(json_encode($row));
    if($row['TYPE_ID'] == $TYPE_CUSTOMER){
        header('location: ./'.$_SESSION['route']);
    }else if($row['TYPE_ID'] == $TYPE_ADMIN){
        header('location: ./admin');
    }else if($row['TYPE_ID'] == $TYPE_SHOP){
        $_SESSION['shop_id'] = $row['shop_id'];
        header('location: ./shop');
    }

}else{
    alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง');
    header('location: ./login.php');
}