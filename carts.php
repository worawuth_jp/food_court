<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    if (!isset($_SESSION['mid'])) {
        alert('กรุณาเข้าสู่ระบบ');
    }
    ?>
</head>
<body>
<?php
include './layout/nav.php';

if (isset($_GET['delete'])) {
    print_r(json_encode($_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']]));
    unset($_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']]);
    if (count($_SESSION['cart'][$_GET['shop_id']]) == 0) {
        unset($_SESSION['cart'][$_GET['shop_id']]);
    }
    header('location: ./carts.php');
}

if (isset($_GET['add'])) {
    print_r(json_encode($_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']]));
    $_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']] += 1;
    header('location: ./carts.php');
}

if (isset($_GET['remove'])) {
    print_r(json_encode($_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']]));
    $_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']] -= 1;
    if ($_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']] == 0) {
        unset($_SESSION['cart'][$_GET['shop_id']][$_GET['food_id']]);
    }
    header('location: ./carts.php');
}
?>
<div class="container self-container">
    <div class="text-center mt-3"><h3>Carts</h3></div>

    <div class="col-md-12 ml-auto mr-auto">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="col-1 text-center" scope="col">#</th>
                <th class="col-4 text-center" scope="col">รายการ</th>
                <th class="col-1 text-center" scope="col">จำนวน</th>
                <th class="col-2 text-center" scope="col">ราคา/หน่วย</th>
                <th class="col-2 text-center" scope="col">รวม</th>
                <th class="col-auto text-center" scope="col">จัดการ</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sum_total = 0;
            if (isset($_SESSION['cart']))
                foreach ($_SESSION['cart'] as $cart_key => $cart_value) {
                    $sql = "SELECT * FROM shop WHERE shop_id='$cart_key'";
                    $result = $conn->query($sql);
                    $row = $result->fetch_assoc();
                    ?>
                    <tr>
                        <th colspan="6" style="color: darkblue" scope="row"><?php echo $row['shop_name'] ?></th>
                    </tr>
                    <?php
                    //print_r($cart_value);
                    $index = 1;
                    foreach ($cart_value as $food_id => $food_num) {
                        $sql = "SELECT * FROM foods WHERE food_id='$food_id'";
                        $result1 = $conn->query($sql);
                        $row1 = $result1->fetch_assoc();
                        ?>
                        <tr>
                            <th scope="row"><?php echo $index++ ?></th>
                            <td><?php echo $row1['food_name'] ?></td>
                            <td class="text-center">
                                <a href="./carts.php?remove=1&food_id=<?php echo $row1['food_id'] ?>&shop_id=<?php echo $row['shop_id'] ?>">
                                    <i class="fas fa-minus-square"></i>
                                </a>
                                <?php echo $food_num ?>
                                <a href="./carts.php?add=1&food_id=<?php echo $row1['food_id'] ?>&shop_id=<?php echo $row['shop_id'] ?>">
                                    <i class="fas fa-plus-square"></i>
                                </a>
                            </td>
                            <td class="text-center"><?php echo $row1['food_price'] ?></td>
                            <td class="text-right"><?php $sum_total += ($row1['food_price'] * $food_num) ;echo number_format($row1['food_price'] * $food_num, 2) ?></td>
                            <td class="text-center">
                                <a href="./carts.php?delete=1&food_id=<?php echo $row1['food_id'] ?>&shop_id=<?php echo $row['shop_id'] ?>">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    <?php
                }
            else{
                ?>
                <tr>
                    <td colspan="6" class="text-center text-muted">No Food in Cart</td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
        <div class="col-md-12 text-right">
            <B>รวม :</B> <span><?php echo ' '.number_format($sum_total,2).' '?></span> <B>บาท</B>
        </div>

        <div class="col-md-12 text-right">
            <a href="./<?php echo $_SESSION['route']?>"><button type="button" class="btn btn-secondary mt-3 ml-auto" name="submit">ย้อนกลับ</button></a>

            <a href="./booking.php"><button class="btn btn-primary mt-3">จองโต๊ะ</button></a>
        </div>
    </div>
</div>
<?php include './layout/footer.php' ?>
</body>
</html>
