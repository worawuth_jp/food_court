<?php
$servername = "localhost";
$username = "root";
$password = "";
$db = "project_test";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);
if ($conn->connect_error) {
    alert("connection failed " . ${$conn->connect_error});
    die("Connection failed: " . $conn->connect_error);
}
