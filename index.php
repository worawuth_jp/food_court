
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    save_route('index.php',$_SESSION);
    $_SESSION['this_route'] = 'index.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div id="carouselExampleIndicators" class="carousel slide mt-4" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block self-image ml-auto mr-auto" src="image/banners/slider-image-1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block self-image ml-auto mr-auto" src="image/banners/slider-image-2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item ">
                <img class="d-block self-image ml-auto mr-auto" src="image/banners/table.jpg" alt="Third slide">
            </div>
        </div>
    </div>

    <div class="text-center mt-3"><h3>ข่าวสาร</h3></div>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
