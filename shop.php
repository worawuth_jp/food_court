<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    save_route('shop.php',$_SESSION);
    $_SESSION['this_route'] = 'shop.php'
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div class="text-center mt-3 mb-4"><h2>ร้านอาหาร</h2></div>
    <div class="row">
    <?php
    $sql = "SELECT * FROM shop";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        ?>
        <div class="col-md-3 mb-3 card ml-3">
            <div class="card-body pl-0 pr-0">
                <img class="img-fluid img-thumbnail img-shop ml-auto mr-auto" src="image/shop/<?php echo $row['pic_shop'] ?>">

                <div class="col-md-12 text-center self-title">
                    <b><?php echo $row['shop_name'] ?></b>
                </div>
                <div class="row mt-2">
                    <a href="./foods.php?shop_id=<?php echo $row['shop_id']?>" class="ml-auto mr-auto">
                        <button type="button" class="btn btn-primary ">เลือกร้านค้า</button>
                    </a>
                </div>

            </div>
        </div>
        <?php
    }
    ?>
    </div>

</div>
<?php include './layout/footer.php' ?>
</body>
</html>
