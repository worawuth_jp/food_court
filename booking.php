<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    save_route('index.php', $_SESSION);
    $_SESSION['this_route'] = 'index.php';
    if (!isset($_SESSION['cart'])) {
        alert('กรุณาเลือกร้านค้าก่อน', 'shop.php');
    }
    if(isset($_GET['empty_table'])){
        alert('ท่านยังไม่ได้เลือกโต๊ะ');
    }
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <form action="./confirm_booking.php" id="thisForm" method="post">
        <?php
        $carts = $_SESSION['cart'];
        foreach ($carts as $shop_id => $foods) {
            $sql = "SELECT * FROM shop 
        WHERE shop_id='$shop_id'";
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();
            ?>
            <div class="text-left mt-3"><h3 style="color: darkblue">จองโต๊ะ ร้านค้า <?php echo $row['shop_name'] ?></h3>
            </div>
            <div class="row pl-3 pr-3">
                <label class="col-form-label">วันเวลาเข้าใช้บริการ : </label>
                <input type="date" class="form-control col-md-2 ml-2"
                       min="1997-01-01" max="2030-12-31"
                       placeholder="dd-mm-yyyy" value="<?php echo date('Y-m-d') ?>"
                       required name="date_<?php echo $shop_id ?>">
                <select class="form-control col-md-1 ml-2" name="from_hour_<?php echo $shop_id ?>">
                    <?php for ($i = 0; $i < 24; $i++) {
                        ?>
                        <option <?php if ($i == 0) echo 'selected' ?>><?php echo $i < 10 ? '0' . $i : $i ?></option>
                        <?php
                    } ?>
                </select>
                <label class="col-form-label ml-1"><B><?= ':' ?></B></label>
                <select class="form-control col-md-1 ml-2" name="from_min_<?php echo $shop_id ?>">
                    <?php for ($i = 0; $i < 60; $i++) {
                        ?>
                        <option <?php if ($i == 0) echo 'selected' ?>><?php echo $i < 10 ? '0' . $i : $i ?></option>
                        <?php
                    } ?>
                </select>
                <label class="col-form-label ml-1"><B><?= 'น.  ถึง  ' ?></B></label>

                <select class="form-control col-md-1 ml-2" name="to_hour_<?php echo $shop_id ?>">
                    <?php for ($i = 0; $i < 24; $i++) {
                        ?>
                        <option <?php if ($i == 0) echo 'selected' ?>><?php echo $i < 10 ? '0' . $i : $i ?></option>
                        <?php
                    } ?>
                </select>
                <label class="col-form-label ml-1"><B><?= ':' ?></B></label>
                <select class="form-control col-md-1 ml-2" name="to_min_<?php echo $shop_id ?>">
                    <?php for ($i = 0; $i < 60; $i++) {
                        ?>
                        <option <?php if ($i == 0) echo 'selected' ?>><?php echo $i < 10 ? '0' . $i : $i ?></option>
                        <?php
                    } ?>
                </select>
                <label class="col-form-label ml-1"><B><?= 'น.' ?></B></label>
                <button type="button" class="btn btn-dark ml-2" onclick="onClick(<?php echo $shop_id ?>)">
                    แสดงโต๊ะที่ว่าง
                </button>
            </div>

            <div class="form-group row mt-3 ml-2">
                <label class="col-form-label">จำนวนคน : </label>
                <div class="col-sm-2">
                    <input type="text" name="num_<?php echo $shop_id ?>" class="form-control" required>
                </div>
            </div>

            <div class="col-md-12 mt-3" id="show_table_<?php echo $shop_id ?>">

            </div>
            <?php

        }
        ?>

        <div class="text-right">
            <a href="./carts.php">
                <button type="button" class="btn btn-secondary mt-3 ml-auto" name="submit">ย้อนกลับ</button>
            </a>
            <button class="btn btn-success mt-3 ml-auto" name="submit">ยืนยันการจอง</button>
        </div>
    </form>

</div>
<?php include './layout/footer.php' ?>
<script>
    function onClick(shop_id) {
        let form = document.getElementById('thisForm');
        const url = './check_table.php';
        let date_in = form.elements[`date_${shop_id}`].value;
        let time_in = form.elements[`from_hour_${shop_id}`].value + ':' + form.elements[`from_min_${shop_id}`].value;
        let time_out = form.elements[`to_hour_${shop_id}`].value + ':' + form.elements[`to_min_${shop_id}`].value;
        $.ajax({
            type: "POST",
            url: url,
            data: {
                date_in: date_in,
                time_in: time_in,
                time_out: time_out,
                shop_id: shop_id
            },
            success: (html) => {
                let content = document.getElementById(`show_table_${shop_id}`);
                content.classList.add('border');
                content.innerHTML = html;
            },
            dataType: 'html'
        });
    }
</script>
</body>
</html>
