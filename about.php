<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    save_route('about.php',$_SESSION);
    $_SESSION['this_route'] = 'about.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';

$sql = "SELECT * FROM about";
$result = $conn->query($sql);
$row = [];
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
}
?>
<div class="container self-container">
    <div id="carouselExampleIndicators" class="carousel slide mt-4" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            $sql = "SELECT about_img FROM about 
INNER JOIN about_img ON about_img.about_id = about.about_id";
            $result = $conn->query($sql);
            $index = 0;
            while ($row1 = $result->fetch_assoc()) {
                ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $index++; ?>"
                    class="<?php echo $index == 0 ? 'active' : '' ?>"></li>
            <?php } ?>
        </ol>
        <div class="carousel-inner">
            <?php
            $sql = "SELECT about_img FROM about 
INNER JOIN about_img ON about_img.about_id = about.about_id";
            $result = $conn->query($sql);
            $index = 0;
            while ($row1 = $result->fetch_assoc()) {
                ?>
                <div class="carousel-item <?php echo $index==0 ? 'active': ''?>">
                    <img class="d-block w-100 ml-auto mr-auto" src="image/about/<?php echo $row1['about_img'] ?>">
                </div>
            <?php $index++; } ?>
        </div>
    </div>

    <div class="row" style="margin-left: auto;margin-right: auto">
        <div class="col-sm-12 text-center" style="padding-top: 20px;padding-bottom: 25px;font-size: 52px">
            <?php echo $row['about_header'] ?>
        </div>
    </div>

    <div class="row" style="margin-left: auto;margin-right: auto">

        <div id="content" class="col-sm-12 pb-5 text-center"
             style="font-size: 18px;margin-left: auto;margin-right: auto;padding-top: 15px;min-height: 100px;width: 100%">
            <?php echo $row['about_content'] ?>
        </div>
    </div>

    <div class="row">
        <video class="col-md-12" width="320" controls autoplay>
            <source src="image/about/<?php echo $row['about_video'] ?>" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    </div>
</div>
<?php include './layout/footer.php' ?>
</body>
</html>
