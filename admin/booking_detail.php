<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php include '../layout/header_admin.php'?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">รายละเอียด ออเดอร์</h1>
            </div>

            <div class="self-content">
                <div class="col-md-12 text-center" style="color: darkblue">
                    <?php
                    $order_id = $_GET['order_id'];
                    $sql = "SELECT * FROM orders INNER JOIN shop ON shop.shop_id = orders.shop_id
WHERE orders.order_id='$order_id'";
                    $result_order = $conn->query($sql);
                    $record = $result_order->fetch_assoc();
                    $d = new DateTime($record['order_date']);
                    $time_in = new DateTime($record['order_time_in']);
                    $time_out = new DateTime($record['order_time_out']);
                    $shop_id = $record['shop_id'];
                    ?>
                    <h5><?= $record['shop_name'] ?> (<?= $d->format('d/m/Y') ?>) เวลา : <?= $time_in->format('H:i') ?> - <?= $time_out->format('H:i') ?></h5>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="col-1 text-center" scope="col">#</th>
                            <th class="col-2 text-center" scope="col">หมายเลขโต๊ะ</th>
                            <th class="col-2 text-center" scope="col">จำนวนที่นั่ง/โต๊ะ</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $sql = "SELECT * FROM `orders` 
INNER JOIN order_tables ON order_tables.order_id = orders.order_id
INNER JOIN tables ON tables.table_id = order_tables.table_id
WHERE orders.order_id = '$order_id' ";
                        $result = $conn->query($sql);
                        if($result->num_rows > 0){
                            while ($row = $result->fetch_assoc()) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $row['table_id'] ?></td>
                                    <td><?php echo $row['table_no'] ?></td>
                                    <td><?php echo $row['table_num'] ?></td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>

                            <td colspan="3">Data Not Found</td>

                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12 mt-3 text-center" style="color: darkblue">
                    <h5>รายการอาหาร</h5>
                </div>

                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="col-1 text-center" scope="col">#</th>
                            <th class="col-4 text-center" scope="col">รายการอาหาร</th>
                            <th class="col-1 text-center" scope="col">จำนวน</th>
                            <th class="col-2 text-center" scope="col">ราคา</th>
                            <th class="col-auto text-center" scope="col">รวม</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $sql = "SELECT order_foods.*,foods.food_name FROM `orders` 
INNER JOIN order_foods ON order_foods.order_id = orders.order_id
INNER JOIN foods ON foods.food_id = order_foods.food_id
WHERE orders.order_id = '$order_id' ";
                        $result = $conn->query($sql);
                        $sum_total = 0;
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $row['food_id'] ?></td>
                                <td class="text-center"><?php echo $row['food_name'] ?></td>
                                <td class="text-center"><?php echo $row['food_num'] ?></td>
                                <td class="text-center"><?php echo $row['food_price'] ?></td>
                                <td class="text-right"><?php $sum_total+=$row['food_price']*$row['food_num']; echo number_format($row['food_price']*$row['food_num'],2) ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td colspan="4" class="text-right">รวมทั้งหมด</td>
                            <td><?= number_format($sum_total,2)?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <B>ความคิดเห็นลูกค้าต่อร้านค้า</B>
                    <div class="col-md-12 border mb-3" style="min-height: 150px">
                        <?php
                        $sql = "SELECT * FROM review_shops WHERE order_id='$order_id'";
                        $res = $conn->query($sql);
                        if($res->num_rows > 0){
                            $row = $res->fetch_assoc();
                            echo $row['review_shop_comment'];
                        }else{
                            echo 'ไม่มีความคิดเห็น';
                        }
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <B>ความคิดเห็นร้านค้าต่อลูก</B>
                    <div class="col-md-12 border mb-3" style="min-height: 150px">
                        <?php
                        $sql = "SELECT * FROM review_customers WHERE order_id='$order_id'";
                        $res = $conn->query($sql);
                        if($res->num_rows > 0){
                            $row = $res->fetch_assoc();
                            echo $row['review_customer_comment'];
                        }else{
                            echo 'ไม่มีความคิดเห็น';
                        }
                        ?>
                    </div>
                </div>

                <div class="col-md-12 text-right">
                    <a href="./index.php">
                        <button class="btn btn-warning ml-2">ออเดอร์ทั้งหมด</button>
                    </a>

                </div>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>