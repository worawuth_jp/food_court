<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'admin/shop.php';
    $pageSize = 10;
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Shops</h1>
            </div>

            <div class="self-content">
                <div class="text-center mt-3"><h3>ร้านอาหารทั้งหมด</h3></div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class=" text-center" scope="col">#</th>
                            <th class="text-center">รูปร้าน</th>
                            <th class="col-6 text-center" scope="col">ร้านอาหาร</th>
                            <th scope="col">เจ้าของร้าน</th>
                            <th class="text-center" scope="col">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT * FROM shop
    INNER JOIN member ON member.member_id = shop.member_id
LIMIT " . $pageSize . " OFFSET " . ($page - 1);
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $row['shop_id'] ?></td>
                                <td class="text-center"><img style="height: 100px" class="img-fluid" src="../image/shop/<?php echo $row['pic_shop']?>" /></td>
                                <td><?php echo $row['shop_name'] ?></td>
                                <td><?php echo '('.$row['member_id'].')'.$row['member_name'] ?></td>
                                <td class="text-center">
                                    <a href="./shop_detail.php?shop_id=<?php echo $row['shop_id'] ?>">
                                        <button class="btn btn-info mr-2">ดูรายละเอียด</button>
                                    </a>

                                </td>

                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">

                        <ul class="pagination justify-content-end">

                            <?php
                            $sql = "SELECT COUNT(*) AS NUM FROM `orders`
INNER JOIN `member` ON `member`.`member_id` = `orders`.`member_id`
INNER JOIN `shop` ON `shop`.`shop_id` = `orders`.`shop_id`
INNER JOIN `payment` ON `payment`.`payment_id`= `orders`.`payment_id`";
                            $result_num = $conn->query($sql);
                            $row = $result_num->fetch_assoc();
                            ?>
                            <li class="page-item <?php echo $page <= 1 ? 'disabled' : '' ?>">
                                <a class="page-link" href="?page=<?= $page - 1; ?>">Previous</a>
                            </li>
                            <?php
                            for ($i = 0; $i < ceil($row['NUM'] / $pageSize); $i++) {
                                ?>
                                <li class="page-item <?php echo ($page == ($i + 1)) ? 'active' : '' ?>"><a
                                        href="?page=<?= $i + 1; ?>" class="page-link" href="#"><?php echo $i + 1; ?></a></li>
                                <?php
                            }
                            ?>

                            <li class="page-item <?php echo ceil($row['NUM'] / $pageSize) == ($page) || $row['NUM'] == 0 ? 'disabled' : '' ?>">
                                <a class="page-link " href="?page=<?= $page + 1; ?>">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>