<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'admin/index.php';
    $pageSize = 10;
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php'?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Orders</h1>
            </div>

            <div class="self-content">
                <div class="text-center mt-3"><h3>ออเดอร์ทั้งหมด</h3></div>

                <div class="row ml-3 mb-3">
                    <div class="card col-md-3 p-0 bg-primary text-white mr-3">
                        <div class="card-body">
                            <div>จำนวนสมาชิกทั้งหมด</div>
                            <div class="col-md-12 text-right">
                                <?php
                                $sql = "SELECT COUNT(*) AS NUM_MEMBER FROM member";
                                $result = $conn->query($sql);
                                $row = $result->fetch_assoc();
                                ?>
                                <h1><?php echo $row['NUM_MEMBER'];?></h1>
                            </div>
                        </div>
                    </div>

                    <div class="card col-md-3 p-0 bg-info mr-3 text-white">
                        <div class="card-body">
                            <div>จำนวนร้านค้า</div>
                            <div class="col-md-12 text-right">
                                <?php
                                $sql = "SELECT COUNT(*) AS NUM_SHOP FROM shop";
                                $result = $conn->query($sql);
                                $row = $result->fetch_assoc();
                                ?>
                                <h1><?php echo $row['NUM_SHOP'];?></h1>
                            </div>
                        </div>
                    </div>

                    <div class="card col-md-3 p-0 bg-success text-white">
                        <div class="card-body">
                            <div>จำนวนออเดอร์ในระบบ</div>
                            <div class="col-md-12 text-right">
                                <?php
                                $sql = "SELECT COUNT(*) AS NUM_ORDER FROM orders WHERE 1";
                                $result = $conn->query($sql);
                                $row = $result->fetch_assoc();
                                ?>
                                <h1><?php echo $row['NUM_ORDER'];?></h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class=" text-center" scope="col">#</th>
                            <th class="col-2 text-center" scope="col">ร้านอาหาร</th>
                            <th class="col-2 text-center" scope="col">วันที่</th>
                            <th class="col-2 text-center" scope="col">เวลาเข้า-ออก</th>
                            <th class="col-2 text-center" scope="col">ลูกค้า</th>
                            <th class="col-2 text-center" scope="col">สถานะ</th>
                            <th class="col-auto text-center" scope="col">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT `orders`.*,`shop`.`shop_name`,`payment`.*,member.* FROM `orders`
INNER JOIN `member` ON `member`.`member_id` = `orders`.`member_id`
INNER JOIN `shop` ON `shop`.`shop_id` = `orders`.`shop_id`
INNER JOIN `payment` ON `payment`.`payment_id`= `orders`.`payment_id`
ORDER BY `orders`.`order_id` DESC
LIMIT " . $pageSize . " OFFSET " . ($page - 1);
                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $row['order_id'] ?></td>
                                <td><?php echo $row['shop_name'] ?></td>
                                <td class="text-center"><?php echo $row['order_date'] ?></td>
                                <td class="text-center"><?php echo $row['order_time_in'] . ' - ' . $row['order_time_out'] ?></td>
                                <td class="text-center"><?php echo '('.$row['member_id'].')'.$row['member_name'] ?></td>
                                <td class="text-center"><?php
                                    if ($row['payment_code'] == $NOT_PAY_STATUS) {
                                        ?>
                                        <span style="color: #909090"><?php echo $row['payment_name'] . '(' . $row['payment_code'] . ')' ?></span>
                                        <?php
                                    } else if ($row['payment_code'] == $PAY_BY_BANK) {
                                        ?>
                                        <span style="color: #46B624"><?php echo $row['payment_name'] . '(' . $row['payment_code'] . ')' ?></span>
                                        <?php
                                    } else if ($row['payment_code'] == $PAY_BY_MONEY) {
                                        ?>
                                        <span style="color: #2B76AF"><?php echo $row['payment_name'] . '(' . $row['payment_code'] . ')' ?></span>
                                        <?php
                                    }
                                    ?></td>
                                <td class="text-center">
                                    <a href="./booking_detail.php?order_id=<?php echo $row['order_id'] ?>">
                                        <button class="btn btn-info mr-2">ดูรายละเอียด</button>
                                    </a>

                                </td>

                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">

                        <ul class="pagination justify-content-end">

                            <?php
                            $sql = "SELECT COUNT(*) AS NUM FROM `orders`
INNER JOIN `member` ON `member`.`member_id` = `orders`.`member_id`
INNER JOIN `shop` ON `shop`.`shop_id` = `orders`.`shop_id`
INNER JOIN `payment` ON `payment`.`payment_id`= `orders`.`payment_id`";
                            $result_num = $conn->query($sql);
                            $row = $result_num->fetch_assoc();
                            ?>
                            <li class="page-item <?php echo $page <= 1 ? 'disabled' : '' ?>">
                                <a class="page-link" href="?page=<?= $page - 1; ?>">Previous</a>
                            </li>
                            <?php
                            for ($i = 0; $i < ceil($row['NUM'] / $pageSize); $i++) {
                                ?>
                                <li class="page-item <?php echo ($page == ($i + 1)) ? 'active' : '' ?>"><a
                                            href="?page=<?= $i + 1; ?>" class="page-link" href="#"><?php echo $i + 1; ?></a></li>
                                <?php
                            }
                            ?>

                            <li class="page-item <?php echo ceil($row['NUM'] / $pageSize) == ($page) || $row['NUM'] == 0 ? 'disabled' : '' ?>">
                                <a class="page-link " href="?page=<?= $page + 1; ?>">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php'?>
</body>
</html>