<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'admin/member.php';
    $pageSize = 10;
    $page=1;
    if(isset($_GET['page'])){
        $page=$_GET['page'];
    }
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php' ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Members</h1>
            </div>

            <div class="self-content mt-3">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">ชื่อสมาชิก</th>
                        <th class="text-center">username</th>
                        <th class="text-center">email</th>
                        <th class="text-center">ที่อยู่</th>
                        <th class="text-center">เบอร์โทรศัพท์</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sql = "SELECT * FROM member 
    INNER JOIN type_member ON type_member.type_member_id = member.type_member_id
    LIMIT ".$pageSize." OFFSET ".($page-1);
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="text-center"><?= $row['member_id'] ?></td>
                                <td ><?= $row['member_name'].' '.$row['member_lastname'] ?></td>
                                <td class="text-center"><?= $row['member_username'] ?></td>
                                <td ><?= $row['member_email'] ?></td>
                                <td ><?= $row['member_address'] ?></td>
                                <td class="text-center"><?= $row['member_phone'] ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>

                <nav aria-label="Page navigation example">

                    <ul class="pagination justify-content-end">

                        <?php
                        $sql = "SELECT COUNT(*) AS NUM FROM member 
    INNER JOIN type_member ON type_member.type_member_id = member.type_member_id";
                        $result_num = $conn->query($sql);
                        $row = $result_num->fetch_assoc();
                        ?>
                        <li class="page-item <?php echo $page <= 1 ? 'disabled' : '' ?>">
                            <a class="page-link" href="?page=<?= $page - 1; ?>">Previous</a>
                        </li>
                        <?php
                        for ($i = 0; $i < ceil($row['NUM'] / $pageSize); $i++) {
                            ?>
                            <li class="page-item <?php echo ($page == ($i + 1)) ? 'active' : '' ?>"><a
                                        href="?page=<?= $i + 1; ?>" class="page-link" href="#"><?php echo $i + 1; ?></a></li>
                            <?php
                        }
                        ?>

                        <li class="page-item <?php echo  ceil($row['NUM'] / $pageSize) == ($page) || $row['NUM'] == 0 ? 'disabled' : '' ?>">
                            <a class="page-link " href="?page=<?= $page + 1; ?>">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php' ?>
</body>
</html>