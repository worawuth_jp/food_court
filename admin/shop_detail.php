<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <?php
    include '../layout/header_admin.php';
    $_SESSION['this_route'] = 'admin/shop_detail.php';
    $shop_id=$_GET['shop_id'];
    ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <?php include '../layout/sidebar.php' ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Foods</h1>
            </div>

            <div class="self-content mt-3">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">รายการอาหาร</th>
                        <th class="text-center">ขนาด</th>
                        <th class="text-center">ราคา</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sql = "SELECT * FROM type_foods WHERE shop_id='$shop_id'";
                    $result = $conn->query($sql);
                    while ($row = $result->fetch_assoc()) {
                        ?>
                        <tr>
                            <td colspan="5"><B><?= $row['type_food_name'] ?></B></td>
                        </tr>
                        <?php
                        $type_food_id = $row['type_food_id'];
                        $sql = "SELECT * FROM foods WHERE type_food_id='$type_food_id'";
                        $result_food = $conn->query($sql);
                        if ($result_food->num_rows > 0) {
                            while ($food = $result_food->fetch_assoc()) {
                                ?>
                                <tr>
                                    <td class="text-center"><?= $food['food_id'] ?></td>
                                    <td><?= $food['food_name'] ?></td>
                                    <td class="text-center"><?= $food['food_size'] ?></td>
                                    <td class="text-center"><?= number_format($food['food_price'], 2) ?></td>

                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center text-muted" colspan="5">ไม่มีรายการอาหาร</td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>

        </main>
    </div>
</div>


<?php include '../layout/footer_admin.php' ?>
</body>
</html>