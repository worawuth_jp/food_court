<?php
require 'constants/index.php';
require 'utils/index.php';
require 'connectDB.php';

if(isset($_POST['member'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $address = $_POST['address'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $type_member_id = $TYPE_CUSTOMER;
    $user_date = date('Y-m-d H:i:s');

    $sql = "INSERT INTO `member`(`type_member_id`, `member_name`, `member_lastname`, `member_username`, `member_password`, `member_date`, `member_email`, `member_address`, `member_phone`) 
VALUES ('$type_member_id','$firstname','$lastname','$username','$password','$user_date','$email','$address','$phone')";
    $result = $conn->query($sql);

    if ($result){
        alert('เพิ่มสมาชิกสำเร็จ','login.php');
    }
}

if(isset($_POST['shop'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $address = $_POST['address'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $type_member_id = $TYPE_SHOP;
    $user_date = date('Y-m-d H:i:s');

    $shop_name = $_POST['shop_name'];
    $random_name = generateRandomString();
    //print_r(json_encode($_FILES));

    $target_dir = "./image/shop/";
    $target_file = 'shop_' .date('YmdHis');
    $pic_img = $target_file.'.png';
    if (move_uploaded_file($_FILES["pic_shop"]["tmp_name"],$target_dir.$pic_img )) {
        $sql = "INSERT INTO `member`(`type_member_id`, `member_name`, `member_lastname`, `member_username`, `member_password`, `member_date`, `member_email`, `member_address`, `member_phone`) 
VALUES ('$type_member_id','$firstname','$lastname','$username','$password','$user_date','$email','$address','$phone')";
        $result = $conn->query($sql);
        if($result){
            $member_id = $conn->insert_id;
            $sql = "INSERT INTO `shop`(`shop_name`, `location`, `pic_shop`, `member_id`) 
VALUES ('$shop_name','','$pic_img','$member_id')";
            $result_shop = $conn->query($sql);
            alert('เพิ่มร้านค้าสำเร็จ','login.php');
        }
    } else{
        alert('เกิดข้อผิดพลาด');
    }
}