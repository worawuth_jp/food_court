
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    $_SESSION['this_route'] = 'booking_success.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';
?>
<div class="container self-container">
    <div class="col-md-12 text-center mt-5">
        <div class="col-md-3 ml-auto mr-auto">
            <img class="img-fluid col-md-12" src="image/success.svg"
                 alt="transparent success picture @transparentpng.com" />
        </div>

    </div>

    <div class="col-md-12 text-center mt-2">
        <div id="self-title-2" style="font-size: 22px">
            การจองสำเร็จ
        </div>
        <a class="btn btn-secondary" href="./index.php">หน้าหลัก</a>
        <a class="btn btn-primary ml-2" href="./order_history.php">การจองของฉัน</a>
    </div>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
