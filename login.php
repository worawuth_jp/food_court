
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';
    $_SESSION['this_route'] = 'login.php';
    ?>
</head>
<body>
<?php
include './layout/nav.php';
//echo $_SESSION['route'];
?>
<div class="container self-container">
    <div class="text-center mt-3"><h3>เข้าสู่ระบบ</h3></div>

    <form class="col-md-6 ml-auto mr-auto" method="post" action="./user_login.php">

        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="userHelp"
                   placeholder="Username">
<!--            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>


        <div class="row pl-3 pr-3 mb-3">
            <a href="./register.php" class="text-underline mr-auto">ยังไม่มีสมาชิก</a>
            <a href="./register_shop.php" class="text-decoration-underline ml-auto">ลงทะเบียนร้านอาหารกับเรา</a>
        </div>

        <div class="col-md-12 text-center p-0">
            <button type="submit" class="btn btn-primary col-md-12 ">Submit</button>
        </div>


    </form>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
