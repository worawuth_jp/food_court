
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>food court</title>
    <?php
    include './layout/header.php';

    $shop_id = '';
    if(isset($_GET['shop_id'])){
        if(!isset($_SESSION['mid'])){
            alert('กรุณาเข้าสู่ระบบ','login.php');
        }
        $shop_id = $_GET['shop_id'];
        save_route('foods.php?shop_id='.$shop_id,$_SESSION);
        $_SESSION['this_route'] = 'foods.php?shop_id='.$shop_id;
        if(isset($_GET['food_id'])){
            $food_id = $_GET['food_id'];
            if(!isset($_SESSION['cart'])){
                $cart = array();
                $obj = array();
                $food = array();
                $food[$food_id] = 1;
                //$obj['foods'] = $food;
                $cart[$shop_id] = $food;
                $_SESSION['cart'] = $cart;
                //print_r(($cart));
            }else{
                $carts = $_SESSION['cart'];
                //unset($_SESSION['cart']);
                foreach ($carts as $shop_cart_id => $foods){
                    if($shop_id != $shop_cart_id){
                        unset($_SESSION['cart'][$shop_cart_id]);
                    }
                }

                $carts = $_SESSION['cart'];
                $shop_found = true;
                $food_found = true;

                if(isset($carts[$shop_id][$food_id])){

                    $carts[$shop_id][$food_id] += 1;
                }
                else{
                    $carts[$shop_id][$food_id] = 1;
                }

                //print_r(json_encode($carts));
                $_SESSION['cart'] = $carts;

            }

        }
    }

    ?>
</head>
<body>
<?php
include './layout/nav.php';

$sql = "SELECT * FROM foods
INNER JOIN type_foods ON type_foods.type_food_id = foods.type_food_id
WHERE type_foods.shop_id='$shop_id'";
$result = $conn->query($sql);
?>
<div class="container self-container">
    <div class="text-center mt-3 mb-3"><h2>อาหาร</h2></div>
    <div class="row">
        <?php
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                ?>
                <div class="col-md-3 mb-3 card ml-3">
                    <div class="card-body pl-0 pr-0">
                        <img class="img-fluid img-thumbnail img-shop ml-auto mr-auto" src="image/foods_img/<?php echo $row['food_img'] ?>">

                        <div class="col-md-12 text-center self-title-food">
                            <b><?php echo $row['food_name'] ?></b>
                        </div>
                        <div class="col-md-12 text-center self-size-food">
                            ขนาด<b><?php echo ' '.$row['food_size'].' ' ?></b>
                        </div>
                        <div class="col-md-12 text-center self-price-food">
                            ราคา<b><?php echo ' '.$row['food_price'].' ' ?></b>บาท
                        </div>
                        <div class="row mt-2">
                            <a href="./foods.php?shop_id=<?php echo $shop_id?>&food_id=<?php echo $row['food_id']?>" class="ml-auto mr-auto">
                                <button type="button" class="btn btn-success ">สั่งอาหาร</button>
                            </a>
                        </div>

                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<?php include './layout/footer.php'?>
</body>
</html>
