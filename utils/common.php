<?php
function console_log($output, $with_script_tags = true)
{
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
        ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

function console_error($output, $with_script_tags = true)
{
    $js_code = 'console.error(' . json_encode($output, JSON_HEX_TAG) .
        ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

function console_warning($output, $with_script_tags = true)
{
    $js_code = 'console.warning(' . json_encode($output, JSON_HEX_TAG) .
        ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

function alert($output, $goto = null, $with_script_tags = true)
{
    $js_code = 'alert(' . json_encode($output, JSON_HEX_TAG) .
        ');';
    if (!is_null($goto)) {
        $js_code = $js_code . 'window.location.href= "./' . $goto . '";';
    }
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }

    echo $js_code;
}

function redirect($path, $with_script_tags = true)
{
    $js_code = 'window.location.href=./' . json_encode($path, JSON_HEX_TAG) . ';';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

function save_route($path_name, &$session)
{
    $session['route'] = $path_name;
}

function isActive(&$session, $name)
{
    return $session['this_route'] == $name ? 'active' : '';
}

function countInCart(&$session)
{
    $count = 0;
    if (isset($session['cart'])) {
        $carts = $session['cart'];
        foreach ($carts as $shop_id => $foods) {
            foreach ($foods as $food_id => $food_num) {
                $count += $food_num;
            }
        }
    }

    return $count;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}